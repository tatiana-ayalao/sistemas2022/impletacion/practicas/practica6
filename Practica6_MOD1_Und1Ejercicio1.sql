﻿  DROP DATABASE IF EXISTS practica6mod1; 
  CREATE DATABASE practica6mod1;
  USE practica6mod1;

CREATE TABLE cliente (
nif int,
nombre varchar(100),
direccion varchar(100),
ciudad varchar(100),
tfno varchar(10),
PRIMARY KEY (nif)
);

CREATE TABLE coche(
matricula varchar(100),
marca varchar(100),
modelo varchar(100),
color varchar(100),
precio float,
PRIMARY KEY (matricula)
);

CREATE TABLE compra (
nifCliente int,
matriculaCoche varchar(100),
PRIMARY KEY (nifCliente, matriculaCoche),
UNIQUE KEY (matriculaCoche)
);

CREATE TABLE revision (
codigo int,
filtro int,
aceite varchar(100),
frenos varchar(100),
matriculaCoche varchar(100)
);


ALTER TABLE compra
ADD CONSTRAINT fkcompraCliente FOREIGN KEY (nifCliente) REFERENCES cliente (nif) ON DELETE RESTRICT ON UPDATE RESTRICT,
ADD CONSTRAINT fkcompraCoche FOREIGN KEY (matriculaCoche) REFERENCES coche (matricula) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE revision 
  ADD CONSTRAINT fkrevisionCoche FOREIGN KEY (matriculaCoche) REFERENCES coche (matricula) ON DELETE RESTRICT ON UPDATE RESTRICT;

