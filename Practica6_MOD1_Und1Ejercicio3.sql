﻿USE practica6mod1;

ALTER TABLE revision
  CHANGE filtro filtro boolean,
  CHANGE aceite aceite boolean,
  CHANGE frenos frenos boolean;

ALTER TABLE coche
  DROP precio;

ALTER TABLE cliente
  CHANGE tfno telefono varchar(10); 

ALTER TABLE compra
  DROP FOREIGN KEY fkcompraCoche,
  DROP KEY matriculaCoche;
  
  ALTER TABLE compra
  ADD CONSTRAINT fkcompraCoche FOREIGN KEY (matriculaCoche) REFERENCES coche (matricula) ON DELETE RESTRICT ON UPDATE RESTRICT;








