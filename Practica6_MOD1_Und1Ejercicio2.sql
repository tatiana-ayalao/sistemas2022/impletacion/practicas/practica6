﻿USE practica6mod1;

INSERT INTO cliente 
  (nif, nombre, direccion, ciudad, tfno) VALUES 
  (2020, 'MARCELO ARCE', 'CALLE NUMANCIA 20', 'Torrelavega', 94234);

SELECT * FROM cliente c;

INSERT INTO coche 
  (matricula, marca, modelo, color, precio) VALUES
  ('3030LXX', 'SUZUKI', 'VITARA', 'AZUL', 20000);

SELECT * FROM coche c;

INSERT INTO compra 
  (nifCliente, matriculaCoche) VALUES
  (2020, '3030LXX');

SELECT * FROM compra c;

INSERT INTO revision 
  (codigo, filtro, aceite, frenos, matriculaCoche) VALUES
  (101,     1, 'Deluxe', 'yes', '3030LXX');

SELECT * FROM revision r;
