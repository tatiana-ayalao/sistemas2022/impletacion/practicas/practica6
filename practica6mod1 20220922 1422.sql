﻿--
-- Script was generated by Devart dbForge Studio 2019 for MySQL, Version 8.2.23.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 22/09/2022 14:22:33
-- Server version: 5.5.5-10.4.24-MariaDB
-- Client version: 4.1
--

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

DROP DATABASE IF EXISTS practica6mod1;

CREATE DATABASE IF NOT EXISTS practica6mod1
CHARACTER SET utf8mb4
COLLATE utf8mb4_general_ci;

--
-- Set default database
--
USE practica6mod1;

--
-- Create table `coche`
--
CREATE TABLE IF NOT EXISTS coche (
  matricula varchar(100) NOT NULL,
  marca varchar(100) DEFAULT NULL,
  modelo varchar(100) DEFAULT NULL,
  color varchar(100) DEFAULT NULL,
  PRIMARY KEY (matricula)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci;

--
-- Create table `revision`
--
CREATE TABLE IF NOT EXISTS revision (
  codigo int(11) DEFAULT NULL,
  filtro tinyint(1) DEFAULT NULL,
  aceite tinyint(1) DEFAULT NULL,
  frenos tinyint(1) DEFAULT NULL,
  matriculaCoche varchar(100) DEFAULT NULL
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci;

--
-- Create foreign key
--
ALTER TABLE revision
ADD CONSTRAINT fkrevisionCoche FOREIGN KEY (matriculaCoche)
REFERENCES coche (matricula);

--
-- Create table `cliente`
--
CREATE TABLE IF NOT EXISTS cliente (
  nif int(11) NOT NULL,
  nombre varchar(100) DEFAULT NULL,
  direccion varchar(100) DEFAULT NULL,
  ciudad varchar(100) DEFAULT NULL,
  telefono varchar(10) DEFAULT NULL,
  PRIMARY KEY (nif)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci;

--
-- Create table `compra`
--
CREATE TABLE IF NOT EXISTS compra (
  nifCliente int(11) NOT NULL,
  matriculaCoche varchar(100) NOT NULL,
  PRIMARY KEY (nifCliente, matriculaCoche)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci;

--
-- Create foreign key
--
ALTER TABLE compra
ADD CONSTRAINT fkcompraCliente FOREIGN KEY (nifCliente)
REFERENCES cliente (nif);

--
-- Create foreign key
--
ALTER TABLE compra
ADD CONSTRAINT fkcompraCoche FOREIGN KEY (matriculaCoche)
REFERENCES coche (matricula);

-- 
-- Dumping data for table coche
--
-- Table practica6mod1.coche does not contain any data (it is empty)

-- 
-- Dumping data for table cliente
--
-- Table practica6mod1.cliente does not contain any data (it is empty)

-- 
-- Dumping data for table revision
--
-- Table practica6mod1.revision does not contain any data (it is empty)

-- 
-- Dumping data for table compra
--
-- Table practica6mod1.compra does not contain any data (it is empty)

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;